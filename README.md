# Dealers and Vehicles Web Client

The application provides a RESTful web client for creating and retrieving auto dealers and their vehicles.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Gradle 4.0](https://gradle.org/releases/)

## Building the application

To build the application execute the following Gradle command:

`$ gradle build`

Test results are available in the `build/test-results` folder.

## Testing the application

To test the application execute the following Gradle command:

`$ gradle test`

Test results are available in the `build/test-results` folder.

## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://github.com/codecentric/springboot-sample-app/blob/master/LICENSE) file.