package com.vauto;

import com.vauto.client.DataSetClient;
import com.vauto.client.DealersClient;
import com.vauto.client.RequestFailedException;
import com.vauto.client.VehiclesClient;
import com.vauto.client.web.jaxrs.DataSetWebClient;
import com.vauto.client.web.jaxrs.DealersWebClient;
import com.vauto.client.web.jaxrs.VehiclesWebClient;
import com.vauto.model.DataSet;
import com.vauto.model.Dealer;
import com.vauto.model.Vehicle;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.time.StopWatch;
import org.glassfish.jersey.internal.guava.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Solution for the vAuto programming challenge.
 */
public class Solution {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private DataSetClient dataSetClient;
    private VehiclesClient vehiclesClient;
    private DealersClient dealersWebClient;

    public Solution(DataSetClient dataSetClient, VehiclesClient vehiclesClient, DealersClient dealersClient) {
        this.dataSetClient = dataSetClient;
        this.vehiclesClient = vehiclesClient;
        this.dealersWebClient = dealersClient;
    }

    public DataSet createDataset() throws RequestFailedException {
        return dataSetClient.createDataSet();
    }

    public List<Dealer> getDealers(String datasetId) throws RequestFailedException {
        log.info("Creating dealer list: datasetId=" + datasetId);

        Map<String, Dealer> dealers = new ConcurrentHashMap<>();
        ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newCachedThreadPool();

        List<String> vehicleIds = vehiclesClient.getVehicles(datasetId);
        for (String vehicleId : vehicleIds) {
            executor.submit(() -> {
                Vehicle vehicle = vehiclesClient.getVehicle(datasetId, vehicleId);
                if (vehicle == null) {
                    log.error("Unable to locate vehicle: datasetId=" + datasetId + ", vehicleId=" + vehicleId);
                    throw new RuntimeException("Unable to locate vehicle: datasetId=" + datasetId +
                            ", vehicleId=" + vehicleId);
                }
                String dealerId = vehicle.getDealerId();
                log.info("Locating vehicle dealer: " + dealerId);
                Dealer dealer = dealers.computeIfAbsent(dealerId, k -> {
                    log.info("Vehicle dealer not loaded. Retrieving dealer: " + dealerId);
                    return dealersWebClient.getDealer(datasetId, k);
                });
                if (dealer == null) {
                    log.error("Unable to locate dealer: datasetId=" + datasetId + ", dealerId=" + dealerId);
                    throw new RuntimeException("Unable to locate dealer: datasetId=" + datasetId +
                            ", dealerId=" + dealerId);
                }
                log.info("Dealer located: " + dealer);
                dealer.addVehicle(vehicle);
                log.info("Vehicle added to dealer: dealerId=" + dealerId + ", vehicle=" + vehicle);
            });
        }
        terminateExecutor(executor);

        List<Dealer> dealerList = Lists.newArrayList(dealers.values());
        log.info("Dealer list created: " + dealerList);
        return dealerList;
    }

    public boolean answer(String datasetId, List<Dealer> dealers) throws RequestFailedException {
        return dataSetClient.answer(datasetId, dealers).isSuccess();
    }

    private void terminateExecutor(ExecutorService executor) {
        executor.shutdown();
        while (!executor.isTerminated()) {
            // nop
        }
    }

    //================================================================================
    // Main Method
    //================================================================================

    public static void main(String[] args) throws ConfigurationException {
        Configurations configs = new Configurations();
        Configuration config = configs.properties(new File("config.properties"));
        URI uri = URI.create(config.getString("dealers.service.host.baseAddress"));

        StopWatch stopWatch = StopWatch.createStarted();

        WebTarget webTarget = ClientBuilder.newClient().target(uri);
        Solution solution = new Solution(
                new DataSetWebClient(webTarget),
                new VehiclesWebClient(webTarget),
                new DealersWebClient(webTarget));
        DataSet dataSet = solution.createDataset();
        List<Dealer> dealers = solution.getDealers(dataSet.getDatasetId());
        solution.answer(dataSet.getDatasetId(), dealers);

        stopWatch.stop();

        System.out.println("Total elapses time: " + stopWatch.getTime(TimeUnit.MILLISECONDS) + "ms");
    }
}