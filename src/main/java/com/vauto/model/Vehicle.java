package com.vauto.model;

import java.util.Objects;

public class Vehicle {
    private String vehicleId;
    private Integer year;
    private String make;
    private String model;
    private String dealerId;

    public Vehicle() {
    }

    public Vehicle(String vehicleId, Integer year, String make, String model, String dealerId) {
        this.vehicleId = vehicleId;
        this.year = year;
        this.make = make;
        this.model = model;
        this.dealerId = dealerId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle)o;
        return getVehicleId().equals(vehicle.getVehicleId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getVehicleId());
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleId='" + vehicleId + '\'' +
                ", year=" + year +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", dealerId='" + dealerId + '\'' +
                '}';
    }
}