package com.vauto.model;

import java.util.Objects;

public class DataSet {
    private String datasetId;

    public String getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(String datasetId) {
        this.datasetId = datasetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataSet dataSet = (DataSet)o;
        return getDatasetId().equals(dataSet.getDatasetId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDatasetId());
    }

    @Override
    public String toString() {
        return "DataSet{" +
                "datasetId='" + datasetId + '\'' +
                '}';
    }
}
