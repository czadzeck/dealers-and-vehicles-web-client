package com.vauto.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Dealer {
    private String dealerId;
    private String name;
    private List<Vehicle> vehicles;

    public Dealer() {
    }

    public Dealer(String dealerId, String name) {
        this.dealerId = dealerId;
        this.name = name;
    }

    public String getDealerId() {
        return dealerId;
    }

    public void setDealerId(String dealerId) {
        this.dealerId = dealerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the <code>List</code> of <code>Vehicle</code>s associated wit this dealer. If
     * no vehicles are present an empty list is returned; <code>null</code> is never returned.
     *
     * @return a <code>List</code> of <code>Vehicle</code>s
     */
    public List<Vehicle> getVehicles() {
        return vehicles == null ? Collections.emptyList() : vehicles;
    }

    public void addVehicle(Vehicle vehicle) {
        if (vehicles == null) {
            vehicles = new ArrayList<>();
        }
        vehicles.add(vehicle);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dealer dealer = (Dealer)o;
        return getDealerId().equals(dealer.getDealerId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDealerId());
    }

    @Override
    public String toString() {
        return "Dealer{" +
                "dealerId='" + dealerId + '\'' +
                ", name='" + name + '\'' +
                ", vehicles=" + vehicles +
                '}';
    }
}