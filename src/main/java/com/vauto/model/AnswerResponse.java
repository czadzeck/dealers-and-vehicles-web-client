package com.vauto.model;

public class AnswerResponse {
    private boolean success;
    private String message;
    private int totalMilliseconds;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public int getTotalMilliseconds() {
        return totalMilliseconds;
    }

    @Override
    public String toString() {
        return "AnswerResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                ", totalMilliseconds=" + totalMilliseconds +
                '}';
    }
}
