package com.vauto.client;

import com.vauto.model.AnswerResponse;
import com.vauto.model.DataSet;
import com.vauto.model.Dealer;

import java.util.List;

/**
 * The <code>DataSetClient</code> interface defines functionality for managing datasets.
 */
public interface DataSetClient {
    /**
     * Creates a <code>DateSet</code>. A dateset is composed of a set of dealers and their vehicles.
     *
     * @return the created <code>DateSet</code>
     *
     * @throws RequestFailedException if the request failed
     */
    DataSet createDataSet();

    /**
     * Submits an answer for the specified dataset in the form of a list of dealers.
     *
     * @param datasetId the dataset identifier of the answer being submitted
     * @param dealers   the answer in the form of a <code>List</code> of <code>Dealer</code>s
     *
     * @return the <code>AnswerResponse</code> indicating the results
     */
    AnswerResponse answer(String datasetId, List<Dealer> dealers);
}