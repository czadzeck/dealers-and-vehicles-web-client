package com.vauto.client.web;

import javax.ws.rs.core.MediaType;
import java.net.URI;

/**
 * The <code>WebClient</code> interface defines a generic web services client.
 */
public interface WebClient {
    /**
     * Returns the target resource URI.
     *
     * @return the target resource URI
     */
    URI getTargetUri();

    /**
     * Returns the default <code>MediaType</code>.
     *
     * @return the default <code>MediaType</code>
     */
    MediaType getDefaultMediaType();
}
