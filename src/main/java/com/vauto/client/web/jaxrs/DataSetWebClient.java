package com.vauto.client.web.jaxrs;

import com.vauto.client.DataSetClient;
import com.vauto.client.RequestFailedException;
import com.vauto.model.AnswerResponse;
import com.vauto.model.DataSet;
import com.vauto.model.Dealer;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

/**
 * A JAX-RS web client implementation of the <code>DataSetClient</code> interface.
 */
public class DataSetWebClient extends AbstractWebClient implements DataSetClient {
    private static final String DATASET_ID_ENDPOINT = "/api/datasetId";
    private static final String DATASET_ID_ANSWER_ENDPOINT = "/api/{datasetId}/answer";

    public DataSetWebClient(WebTarget webTarget) {
        super(webTarget, MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    public DataSet createDataSet() throws RequestFailedException {
        log.debug("Creating dataset");
        Response response;
        try {
            response = createInvocationBuilder(DATASET_ID_ENDPOINT).get();
        } catch (Exception e) {
            log.error("Exception encountered creating dataset", e);
            throw new RequestFailedException("Failed creating dataset", e);
        }

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            log.error("Failed creating dataset: " + response);
            throw new RequestFailedException("Failed to create dataset", response);
        }

        DataSet dataSet;
        try {
            dataSet = response.readEntity(DataSet.class);
        } catch (Exception e) {
            log.error("Exception encountered creating dataset", e);
            throw new RequestFailedException("Failed creating dataset", e);
        }
        log.info("dataset created: " + dataSet);
        return dataSet;
    }

    @Override
    public AnswerResponse answer(String datasetId, List<Dealer> dealers) throws RequestFailedException {
        log.debug("Submitting answer: datasetId=" + datasetId + ", dealers=" + dealers);
        Response response;
        try {
            response = createInvocationBuilder(
                    DATASET_ID_ANSWER_ENDPOINT.replace("{datasetId}", datasetId))
                    .post(Entity.entity(new Dealers(dealers), MediaType.APPLICATION_JSON_TYPE));
        } catch (Exception e) {
            log.error("Exception encountered submitting answer", e);
            throw new RequestFailedException("Failed to submit answer: datasetId=" + datasetId, e);
        }

        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            log.error("Failed submitting answer: " + response);
            throw new RequestFailedException("Failed to submit answer: datasetId=" + datasetId, response);
        }

        AnswerResponse answerResponse;
        try {
            answerResponse = response.readEntity(AnswerResponse.class);
        } catch (Exception e) {
            log.error("Exception encountered submitting answer", e);
            throw new RequestFailedException("Failed to submit answer: datasetId=" + datasetId, e);
        }
        log.info("Answer submitted: datasetId=" + datasetId + ", response=" + answerResponse);
        return answerResponse;
    }

    //================================================================================
    // Inner Classes
    //================================================================================

    private static class Dealers {
        private List<Dealer> dealers;

        public Dealers() {
        }

        public Dealers(List<Dealer> dealers) {
            this.dealers = dealers;
        }

        public List<Dealer> getDealers() {
            return dealers == null ? Collections.emptyList() : dealers;
        }
    }
}