package com.vauto.client.web.jaxrs;

import com.vauto.client.web.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.net.URI;

/**
 * This class provides a skeletal JAX-RS implementation of the <code>WebClient</code> interface.
 */
public abstract class AbstractWebClient implements WebClient {
    protected final Logger log = LoggerFactory.getLogger(getClass());
    private WebTarget webTarget;
    private MediaType defaultMediaType;

    public AbstractWebClient(WebTarget webTarget, MediaType defaultMediaType) {
        this.webTarget = webTarget;
        this.defaultMediaType = defaultMediaType;
    }

    @Override
    public URI getTargetUri() {
        return webTarget.getUri();
    }

    @Override
    public MediaType getDefaultMediaType() {
        return defaultMediaType;
    }

    /**
     * Creates an <code>Invocation.Builder</code> with the specified path and the default media type.
     *
     * @param path the path of the web resource
     *
     * @return the newly created <code>Invocation.Builder</code>
     */
    protected Invocation.Builder createInvocationBuilder(String path) {
        return createInvocationBuilder(path, getDefaultMediaType());
    }

    /**
     * Creates an <code>Invocation.Builder</code> with the specified path and media type.
     *
     * @param path      the path of the web resource
     * @param mediaType the accepted response media type
     *
     * @return the newly created <code>Invocation.Builder</code>
     */
    protected Invocation.Builder createInvocationBuilder(String path, MediaType mediaType) {
        return webTarget
                .path(path)
                .request(mediaType);
    }
}