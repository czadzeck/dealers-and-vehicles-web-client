package com.vauto.client.web.jaxrs;

import com.vauto.client.DealersClient;
import com.vauto.client.RequestFailedException;
import com.vauto.model.Dealer;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * A JAX-RS web client implementation of the <code>DealersClient</code> interface.
 */
public class DealersWebClient extends AbstractWebClient implements DealersClient {
    private static final String DEALERS_ENDPOINT = "/api/{datasetId}/dealers/{dealerId}";

    public DealersWebClient(WebTarget webTarget) {
        super(webTarget, MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    public Dealer getDealer(String datasetId, String dealerId) throws RequestFailedException {
        log.debug("Retrieving dealer: datasetId=" + datasetId + ", dealerId=" + dealerId);
        Response response;
        try {
            response = createInvocationBuilder(
                    DEALERS_ENDPOINT
                            .replace("{datasetId}", datasetId)
                            .replace("{dealerId}", dealerId))
                    .get();
        } catch (Exception e) {
            log.error("Exception encountered retrieving dealer", e);
            throw new RequestFailedException("Failed to retrieve dealer: datasetId=" + datasetId +
                    ", dealerId=" + dealerId, e);
        }

        if (response.getStatus() == Status.NOT_FOUND.getStatusCode()) {
            return null;
        } else if (response.getStatus() != Status.OK.getStatusCode()) {
            log.error("Failed retrieving dealer: datasetId=" + datasetId +
                    ", dealerId=" + dealerId + ", response=" + response);
            throw new RequestFailedException("Failed to retrieve dealer: datasetId=" + datasetId +
                    ", dealerId=" + dealerId, response);
        }

        Dealer dealer;
        try {
            dealer = response.readEntity(Dealer.class);
        } catch (Exception e) {
            log.error("Exception encountered retrieving dealer", e);
            throw new RequestFailedException("Failed to retrieve dealer: datasetId=" + datasetId +
                    ", dealerId=" + dealerId, e);
        }
        log.info("Dealer retrieved: datasetId=" + datasetId + ", dealer=" + dealer);
        return dealer;
    }
}