package com.vauto.client.web.jaxrs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vauto.client.RequestFailedException;
import com.vauto.model.Vehicle;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.Collections;
import java.util.List;

/**
 * A JAX-RS web client implementation of the <code>VehiclesWebClient</code> interface.
 */
public class VehiclesWebClient extends AbstractWebClient implements com.vauto.client.VehiclesClient {
    private static final String VEHICLES_ENDPOINT = "/api/{datasetId}/vehicles";
    private static final String VEHICLE_ENDPOINT = "/api/{datasetId}/vehicles/{vehicleId}";

    public VehiclesWebClient(WebTarget webTarget) {
        super(webTarget, MediaType.APPLICATION_JSON_TYPE);
    }

    @Override
    public List<String> getVehicles(String datasetId) throws RequestFailedException {
        log.debug("Retrieving vehicle identifiers: datasetId=" + datasetId);
        Response response;
        try {
            response = createInvocationBuilder(
                    VEHICLES_ENDPOINT.replace("{datasetId}", datasetId)).get();
        } catch (Exception e) {
            log.error("Exception encountered retrieving vehicles", e);
            throw new RequestFailedException("Failed to retrieve vehicles: " + datasetId, e);
        }

        if (response.getStatus() != Status.OK.getStatusCode()) {
            log.error("Failed retrieving vehicles: datasetId=" + datasetId + ", response=" + response);
            throw new RequestFailedException("Failed to retrieve vehicles: " + datasetId, response);
        }

        List<String> vehicleIdentifiers;
        try {
            vehicleIdentifiers = response.readEntity(Vehicles.class).getVehicles();
        } catch (Exception e) {
            log.error("Exception encountered retrieving vehicles", e);
            throw new RequestFailedException("Failed to retrieve vehicles: " + datasetId, e);
        }
        log.info("Vehicle identifiers retrieved: datasetId=" + datasetId + ", vehicles=" + vehicleIdentifiers);
        return vehicleIdentifiers == null ? Collections.emptyList() : vehicleIdentifiers;
    }

    @Override
    public Vehicle getVehicle(String datasetId, String vehicleId) throws RequestFailedException {
        log.debug("Retrieving vehicle: datasetId=" + datasetId + ", vehicleId=" + vehicleId);
        Response response;
        try {
            response = createInvocationBuilder(
                    VEHICLE_ENDPOINT
                            .replace("{datasetId}", datasetId)
                            .replace("{vehicleId}", vehicleId))
                    .get();
        } catch (Exception e) {
            log.error("Exception encountered retrieving vehicle", e);
            throw new RequestFailedException("Failed to retrieve vehicle: " + datasetId +
                    ", vehicleId=" + vehicleId, e);
        }

        if (response.getStatus() == Status.NOT_FOUND.getStatusCode()) {
            return null;
        } else if (response.getStatus() != Status.OK.getStatusCode()) {
            log.error("Failed retrieving vehicle: datasetId=" + datasetId +
                    ", vehicleId=" + vehicleId + ", response=" + response);
            throw new RequestFailedException("Failed to retrieve vehicle: datasetId=" + datasetId +
                    ", vehicleId=" + vehicleId, response);
        }

        Vehicle vehicle;
        try {
            vehicle = response.readEntity(Vehicle.class);
        } catch (Exception e) {
            log.error("Exception encountered retrieving vehicle", e);
            throw new RequestFailedException("Failed to retrieve vehicle: " + datasetId +
                    ", vehicleId=" + vehicleId, e);
        }
        log.info("Vehicle retrieved: datasetId=" + datasetId + ", vehicle=" + vehicle);
        return vehicle;
    }

    //================================================================================
    // Inner Classes
    //================================================================================

    private static class Vehicles {
        @JsonProperty("vehicleIds")
        private List<String> vehicles;

        public List<String> getVehicles() {
            return vehicles == null ? Collections.emptyList() : vehicles;
        }
    }
}