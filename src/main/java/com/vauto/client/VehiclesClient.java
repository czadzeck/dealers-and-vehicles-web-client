package com.vauto.client;

import com.vauto.model.Vehicle;

import java.util.List;

/**
 * The <code>VehiclesWebClient</code> interface defines functionality for managing vehicles.
 */
public interface VehiclesClient {
    /**
     * Returns a <code>List</code> of vehicle identifiers for the specified dataset
     * identifier. If no vehicles are present an empty list is returned; <code>null</code>
     * is never returned.
     *
     * @param datasetId the dataset identifier of the vehicles to retrieve
     *
     * @return a <code>List</code> of vehicle identifiers
     *
     * @throws RequestFailedException if the request failed or the dataset identifier is invalid
     */
    List<String> getVehicles(String datasetId);

    /**
     * Returns the <code>Vehicle</code> associated with the specified dataset and vehicle identifiers.
     *
     * @param datasetId the dataset identifier of the vehicle to retrieve
     * @param vehicleId the identifier of the vehicle to retrieve
     *
     * @return the <code>Vehicle</code>. If the vehicle does not exists, <code>null</code> is returned
     *
     * @throws RequestFailedException if the request failed or the dataset identifier is invalid
     */
    Vehicle getVehicle(String datasetId, String vehicleId);
}
