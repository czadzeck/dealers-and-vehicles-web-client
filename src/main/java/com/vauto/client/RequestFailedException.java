package com.vauto.client;

/**
 * This exception indicates that an error was encountered during a web request.
 */
public class RequestFailedException extends RuntimeException {
    private Object response;

    /**
     * Construct a <coded>RequestFailedException</coded> with the specified response object.
     *
     * @param response the response to the web request
     */
    public RequestFailedException(Object response) {
        this.response = response;
    }

    /**
     * Construct a <coded>RequestFailedException</coded> with the specified message and response object.
     *
     * @param message  a description of the exception
     * @param response the response to the web request
     */
    public RequestFailedException(String message, Object response) {
        super(message);
        this.response = response;
    }

    /**
     * Construct a <coded>RequestFailedException</coded> with the specified message and the cause.
     *
     * @param message a description of the exception
     * @param cause   the cause of the failure
     */
    public RequestFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Returns the web client response of this exception or <code>null</code> if the it is nonexistent or unknown.
     *
     * @return the web client response of this exception or <code>null</code> if the it is nonexistent or unknown
     */
    public Object getResponse() {
        return response;
    }
}
