package com.vauto.client;

import com.vauto.model.Dealer;

/**
 * The <code>DealersClient</code> interface defines functionality for managing dealers.
 */
public interface DealersClient {
    /**
     * Returns the <code>Dealer</code> associated with the specified dataset and dealer identifiers.
     *
     * @param datasetId the dataset identifier of the dealer to retrieve
     * @param dealerId  the identifier of the dealer to retrieve
     *
     * @return the <code>Dealer</code>. If the dealer does not exists, <code>null</code> is returned
     *
     * @throws RequestFailedException if the request failed or the dataset identifier is invalid
     */
    Dealer getDealer(String datasetId, String dealerId);
}