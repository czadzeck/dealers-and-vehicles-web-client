package com.vauto;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.vauto.client.DataSetClient;
import com.vauto.client.DealersClient;
import com.vauto.client.web.jaxrs.DataSetWebClient;
import com.vauto.client.web.jaxrs.DealersWebClient;
import com.vauto.client.web.jaxrs.VehiclesWebClient;
import com.vauto.model.DataSet;
import com.vauto.model.Dealer;
import com.vauto.model.Vehicle;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.client.ClientBuilder;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.*;

public class SolutionTest {
    @Rule
    public WireMockRule wm = new WireMockRule(
            wireMockConfig().dynamicPort().dynamicHttpsPort()
    );
    private DataSetClient dataSetClient;
    private DealersClient dealersClient;
    private VehiclesWebClient vehiclesClient;

    private static Dealer findDealer(String dealerId, List<Dealer> dealers) {
        for (Dealer dealer : dealers) {
            if (dealer.getDealerId().equals(dealerId)) {
                return dealer;
            }
        }
        return null;
    }

    @Before
    public void setup() {
        dataSetClient = new DataSetWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));

        dealersClient = new DealersWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));

        vehiclesClient = new VehiclesWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));

        configureFor("localhost", wm.port());

        initializeMockStubs();
    }

    @Test
    public void testDataSetCreation() {
        Solution solution = new Solution(dataSetClient, vehiclesClient, dealersClient);
        DataSet dataSet = solution.createDataset();

        assertEquals(dataSet.getDatasetId(), "imyXK1if1gg");
    }

    @Test
    public void testGetDealers() {
        Solution solution = new Solution(dataSetClient, vehiclesClient, dealersClient);
        DataSet dataSet = solution.createDataset();
        List<Dealer> dealers = solution.getDealers(dataSet.getDatasetId());

        assertNotNull(dealers);
        assertFalse(dealers.isEmpty());
        assertEquals(dealers.size(), 2);

        // ACME Auto
        Dealer dealer1 = findDealer("1998950008", dealers);
        assertNotNull(dealer1);
        assertEquals(dealer1.getDealerId(), "1998950008");
        assertEquals(dealer1.getName(), "ACME Auto");

        List<Vehicle> dealer1Vehicles = dealer1.getVehicles();
        assertNotNull(dealer1Vehicles);
        assertEquals(dealer1Vehicles.size(), 1);

        Vehicle vehicle1 = dealer1Vehicles.get(0);
        assertEquals(vehicle1.getVehicleId(), "1429665002");
        assertEquals(vehicle1.getYear(), Integer.valueOf(1908));
        assertEquals(vehicle1.getMake(), "Ford");
        assertEquals(vehicle1.getModel(), "Model T");
        assertEquals(vehicle1.getDealerId(), "1998950008");

        // House of Wheels
        Dealer dealer2 = findDealer("969779322", dealers);
        assertNotNull(dealer2);
        assertEquals(dealer2.getDealerId(), "969779322");
        assertEquals(dealer2.getName(), "House of Wheels");

        List<Vehicle> dealer2Vehicles = dealer2.getVehicles();
        assertNotNull(dealer2Vehicles);
        assertEquals(dealer2Vehicles.size(), 1);

        Vehicle vehicle2 = dealer2Vehicles.get(0);
        assertEquals(vehicle2.getVehicleId(), "1429665002");
        assertEquals(vehicle2.getYear(), Integer.valueOf(2014));
        assertEquals(vehicle2.getMake(), "Ford");
        assertEquals(vehicle2.getModel(), "F150");
        assertEquals(vehicle2.getDealerId(), "969779322");
    }

    @Test
    public void testCorrectAnswer() {
        Solution solution = new Solution(dataSetClient, vehiclesClient, dealersClient);
        DataSet dataSet = solution.createDataset();
        List<Dealer> dealers = solution.getDealers(dataSet.getDatasetId());
        boolean answerCorrect = solution.answer(dataSet.getDatasetId(), dealers);

        assertTrue(answerCorrect);
    }

    //================================================================================
    // Private Helper Methods
    //================================================================================

    private void initializeMockStubs() {
        stubFor(get(urlEqualTo("/api/datasetId"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"datasetId\" : \"imyXK1if1gg\"}")
                )
        );

        stubFor(get(urlEqualTo("/api/imyXK1if1gg/vehicles"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"vehicleIds\" : [" +
                                "1429665002," +
                                "460197671]}")
                )
        );

        stubFor(get(urlEqualTo("/api/imyXK1if1gg/vehicles/1429665002"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"vehicleId\" : 1429665002," +
                                "\"year\" : 1908," +
                                "\"make\" : \"Ford\"," +
                                "\"model\" : \"Model T\"," +
                                "\"dealerId\" : 1998950008}")
                )
        );

        stubFor(get(urlEqualTo("/api/imyXK1if1gg/vehicles/460197671"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"vehicleId\" : 1429665002," +
                                "\"year\" : 2014," +
                                "\"make\" : \"Ford\"," +
                                "\"model\" : \"F150\"," +
                                "\"dealerId\" : 969779322}")
                )
        );

        stubFor(get(urlEqualTo("/api/imyXK1if1gg/dealers/1998950008"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"dealerId\" : 1998950008," +
                                "\"name\" : \"ACME Auto\"}")
                )
        );

        stubFor(get(urlEqualTo("/api/imyXK1if1gg/dealers/969779322"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"dealerId\" : 969779322," +
                                "\"name\" : \"House of Wheels\"}")
                )
        );

        stubFor(post(urlEqualTo("/api/imyXK1if1gg/answer"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"success\" : true," +
                                "\"message\" : \"You did it!\"," +
                                "\"totalMilliseconds\" : 1000}")
                )
        );
    }
}
