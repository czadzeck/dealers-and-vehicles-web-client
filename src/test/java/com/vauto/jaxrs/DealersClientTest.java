package com.vauto.jaxrs;

import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.vauto.client.DealersClient;
import com.vauto.client.RequestFailedException;
import com.vauto.client.web.jaxrs.DealersWebClient;
import com.vauto.model.Dealer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.*;

public class DealersClientTest {
    @Rule
    public WireMockRule wm = new WireMockRule(
            wireMockConfig().dynamicPort().dynamicHttpsPort()
    );

    private DealersClient dealersClient;

    @Before
    public void setup() {
        dealersClient = new DealersWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));
        configureFor("localhost", wm.port());
    }

    @Test
    public void testRetrievalOfValidDealer() {
        stubFor(get(urlEqualTo("/api/1/dealers/2"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"dealerId\" : 1," +
                                "\"name\" : \"ACME Auto\"}")
                )
        );

        Dealer dealer = dealersClient.getDealer("1", "2");

        assertEquals(dealer.getDealerId(), "1");
        assertEquals(dealer.getName(), "ACME Auto");
        assertNotNull(dealer.getVehicles());
        assertTrue(dealer.getVehicles().isEmpty());
    }

    @Test
    public void testRetrievalOfInvalidDealer() {
        stubFor(get(urlEqualTo("/api/1/dealers/3"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(404))
        );

        Dealer dealer = dealersClient.getDealer("1", "3");

        assertNull(dealer);
    }

    @Test
    public void testDealerRetrievalWithInvalidDataSet() {
        stubFor(get(urlEqualTo("/api/2/dealers/1"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dealersClient.getDealer("2", "1");
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);
    }

    @Test
    public void testDealerRetrievalWithInternalServerError() {
        stubFor(get(urlEqualTo("/api/3/dealers/5"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dealersClient.getDealer("3", "5");
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);

    }

    @Test
    public void testDealerRetrievalWithInvalidResponseEntity() {
        stubFor(get(urlEqualTo("/api/4/dealers/2"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"userName\" : \"Luke Skywalker\"}")
                )
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dealersClient.getDealer("4", "2");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testDealerRetrievalWithConnectionFault() {
        stubFor(get(urlEqualTo("/api/2/dealers/1"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dealersClient.getDealer("2", "1");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

}