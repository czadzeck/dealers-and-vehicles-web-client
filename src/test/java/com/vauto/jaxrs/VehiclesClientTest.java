package com.vauto.jaxrs;

import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.vauto.client.RequestFailedException;
import com.vauto.client.VehiclesClient;
import com.vauto.client.web.jaxrs.VehiclesWebClient;
import com.vauto.model.Vehicle;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.*;

public class VehiclesClientTest {
    @Rule
    public WireMockRule wm = new WireMockRule(
            wireMockConfig().dynamicPort().dynamicHttpsPort()
    );

    private VehiclesClient vehiclesClient;

    @Before
    public void setup() {
        vehiclesClient = new VehiclesWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));
        configureFor("localhost", wm.port());
    }

    @Test
    public void testRetrievalOfValidVehicle() {
        stubFor(get(urlEqualTo("/api/543/vehicles/23245"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"vehicleId\" : 23245," +
                                "\"year\" : 1908," +
                                "\"make\" : \"Ford\"," +
                                "\"model\" : \"Model T\"," +
                                "\"dealerId\" : 543}")
                )
        );

        Vehicle vehicle = vehiclesClient.getVehicle("543", "23245");

        assertEquals(vehicle.getVehicleId(), "23245");
        assertEquals(vehicle.getYear(), Integer.valueOf(1908));
        assertEquals(vehicle.getMake(), "Ford");
        assertEquals(vehicle.getModel(), "Model T");
        assertEquals(vehicle.getDealerId(), "543");
    }

    @Test
    public void testRetrievalOfInvalidVehicle() {
        stubFor(get(urlEqualTo("/api/542438/vehicles/8744326"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(404))
        );

        Vehicle vehicle = vehiclesClient.getVehicle("542438", "8744326");

        assertNull(vehicle);
    }

    @Test
    public void testVehicleRetrievalWithInternalServerError() {
        stubFor(get(urlEqualTo("/api/10/vehicles/1001"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicle("10", "1001");
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);
    }

    @Test
    public void testVehicleRetrievalWithInvalidResponseEntity() {
        stubFor(get(urlEqualTo("/api/25/vehicles/51001"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"itemCount\" : 5542}")
                )
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicle("25", "51001");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testDVehicleRetrievalWithConnectionFault() {
        stubFor(get(urlEqualTo("/api/230/vehicles/43402"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicle("230", "43402");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testRetrievalOfValidVehicles() {
        stubFor(get(urlEqualTo("/api/imyXK1if1gg/vehicles"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"vehicleIds\" : [" +
                                "1429665002," +
                                "1778814325," +
                                "1902764257," +
                                "1777503064," +
                                "460197671]}")
                )
        );

        List<String> vehicles = vehiclesClient.getVehicles("imyXK1if1gg");

        assertNotNull(vehicles);
        assertFalse(vehicles.isEmpty());
        assertEquals(vehicles.size(), 5);
        assertTrue(vehicles.contains("1429665002"));
        assertTrue(vehicles.contains("1778814325"));
        assertTrue(vehicles.contains("1902764257"));
        assertTrue(vehicles.contains("1777503064"));
        assertTrue(vehicles.contains("460197671"));
    }

    @Test
    public void testRetrievalOfDatasetWithNoVehicles() {
        stubFor(get(urlEqualTo("/api/gfjg49ghgh/vehicles"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"vehicleIds\" : []}")
                )
        );

        List<String> vehicles = vehiclesClient.getVehicles("gfjg49ghgh");

        assertNotNull(vehicles);
        assertTrue(vehicles.isEmpty());
    }

    @Test
    public void testVehiclesRetrievalWithInternalServerError() {
        stubFor(get(urlEqualTo("/api/gfd74j7487f/vehicles"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicles("gfd74j7487f");
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);
    }

    @Test
    public void testVehiclesRetrievalWithInvalidResponseEntity() {
        stubFor(get(urlEqualTo("/api/j45j94/vehicles"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"foodType\" : \"Dried Up Old Hamburger\"}")
                )
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicles("j45j94");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testDVehiclesRetrievalWithConnectionFault() {
        stubFor(get(urlEqualTo("/api/ghfdh65df/vehicles"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            vehiclesClient.getVehicles("ghfdh65df");
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }
}