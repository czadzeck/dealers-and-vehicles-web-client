package com.vauto.jaxrs;

import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.vauto.client.DataSetClient;
import com.vauto.client.RequestFailedException;
import com.vauto.client.web.jaxrs.DataSetWebClient;
import com.vauto.model.AnswerResponse;
import com.vauto.model.DataSet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.Collections;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.*;

public class DataSetClientTest {
    @Rule
    public WireMockRule wm = new WireMockRule(
            wireMockConfig().dynamicPort().dynamicHttpsPort()
    );

    private DataSetClient dataSetClient;

    @Before
    public void setup() {
        dataSetClient = new DataSetWebClient(ClientBuilder.newClient()
                .target("http://localhost:" + wm.port()));
        configureFor("localhost", wm.port());
    }

    //================================================================================
    // Dataset Tests
    //================================================================================

    @Test
    public void testDataSetCreation() {
        stubFor(get(urlEqualTo("/api/datasetId"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"datasetId\" : \"1\"}")
                )
        );

        DataSet dataSet = dataSetClient.createDataSet();

        assertEquals(dataSet.getDatasetId(), "1");
    }

    @Test
    public void testDataSetCreationWithInternalServerError() {
        stubFor(get(urlEqualTo("/api/datasetId"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.createDataSet();
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);
    }

    @Test
    public void testDataSetCreationWithInvalidResponseEntity() {
        stubFor(get(urlEqualTo("/api/datasetId"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"customerId\" : \"458034\"}")
                )
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.createDataSet();
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testDataSetCreationWithConnectionFault() {
        stubFor(get(urlEqualTo("/api/datasetId"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.createDataSet();
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    //================================================================================
    // Answer Tests
    //================================================================================

    @Test
    public void testCorrectAnswer() {
        stubFor(post(urlEqualTo("/api/1/answer"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"success\" : true," +
                                "\"message\" : \"You did it!\"," +
                                "\"totalMilliseconds\" : 1000}")
                )
        );

        AnswerResponse response = dataSetClient.answer("1", Collections.emptyList());

        assertTrue(response.isSuccess());
        assertEquals(response.getMessage(), "You did it!");
        assertEquals(response.getTotalMilliseconds(), 1000);
    }

    @Test
    public void testIncorrectAnswer() {
        stubFor(post(urlEqualTo("/api/2/answer"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{" +
                                "\"success\" : false," +
                                "\"message\" : \"You have failed!\"," +
                                "\"totalMilliseconds\" : 500}")
                )
        );

        AnswerResponse response = dataSetClient.answer("2", Collections.emptyList());

        assertFalse(response.isSuccess());
        assertEquals(response.getMessage(), "You have failed!");
        assertEquals(response.getTotalMilliseconds(), 500);
    }

    @Test
    public void testAnswerWithInternalServerError() {
        stubFor(post(urlEqualTo("/api/3/answer"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse().withStatus(500))
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.answer("3", Collections.emptyList());
        });

        assertNotNull(exception.getResponse());
        assertTrue(exception.getResponse() instanceof Response);
        assertEquals(((Response)exception.getResponse()).getStatus(), 500);
    }

    @Test
    public void testAnswerWithInvalidResponseEntity() {
        stubFor(post(urlEqualTo("/api/4/answer"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"customerId\" : \"458034\"}")
                )
        );

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.answer("4", Collections.emptyList());
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }

    @Test
    public void testAnswerWithConnectionFault() {
        stubFor(post(urlEqualTo("/api/5/answer"))
                .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        RequestFailedException exception = assertThrows(RequestFailedException.class, () -> {
            dataSetClient.answer("5", Collections.emptyList());
        });

        assertNull(exception.getResponse());
        assertTrue(exception.getCause() instanceof ProcessingException);
    }
}